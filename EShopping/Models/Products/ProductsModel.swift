//
//  ProductsModel.swift
//  EShopping
//
//  Created by ITROPE TECHNOLOIES on 22/11/19.
//  Copyright © 2019 ITROPE TECHNOLOIES. All rights reserved.
//

import Foundation
class NormalProducts : Codable{
    var id : Int
    var name : String?
    var type : String?
    var price: Int
    
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case type
        case price
    }
    
    required init(from decoder: Decoder) throws {
        let container = try?decoder.container(keyedBy: CodingKeys.self)
        id = try (container?.decode(Int.self, forKey: .id) ?? 0)
        name = try container?.decode(String.self, forKey: .name)
        type = try container?.decode(String.self, forKey: .type)
        price = try (container?.decode(Int.self, forKey: .price) ?? 0)
    }
    
    init() {
        self.id = 0
        self.price = 0
        
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode( name, forKey: .name)
        try container.encode(type, forKey: .type)
        try container.encode(price, forKey: .price)
      
    }
}

class PrimeProducts : NormalProducts{
    
    var prime_discount : Int
    
    
    private enum CodingKeys: String, CodingKey {
     
        case prime_discount
    }
    
    required init(from decoder: Decoder) throws {
        let container = try? decoder.container(keyedBy: CodingKeys.self)
        prime_discount = try (container?.decode(Int.self, forKey: .prime_discount) ?? 0)
        try super.init(from: decoder)
    }
    
    override init() {
        self.prime_discount = 0
          super.init()
    }
    override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try? container.encode(prime_discount, forKey: .prime_discount)

      
    }
}
