//
//  EncoderDecoder.swift
//  EShopping
//
//  Created by ITROPE TECHNOLOIES on 22/11/19.
//  Copyright © 2019 ITROPE TECHNOLOIES. All rights reserved.
//

import Foundation
class EncoderDecoder {
    
    let decoder = JSONDecoder()
    let encoder = JSONEncoder()
    let localeStr = "en_US_POSIX"
    
    func decode<T>(_ type: T.Type, from data: Data) throws -> T where T: Decodable {
        return try self.decoder.decode(type, from: data)
    }
    
    func encode<T>(_ value: T) throws -> Data where T: Encodable {
        return try self.encoder.encode(value)
    }
    
    func encodeAndGetDict<T>(_ value: T) throws -> [String: Any]? where T: Encodable {
        let encodedData = try self.encoder.encode(value)
        return try JSONSerialization.jsonObject(with: encodedData, options: []) as? [String: Any]
    }
    
    func encodeAndGetArray<T>(_ value: T) throws -> [[String: Any]]? where T: Encodable {
        let encodedData = try self.encoder.encode(value)
        return try JSONSerialization.jsonObject(with: encodedData, options: []) as? [[String: Any]]
    }
}
